# recuperacion-appsweb

Repositorio para el examen de recuperación de la asignatura "Aplicaciones Web", del 3er Cuatrimestre, impartido en la UTH


# Contributors

- Alonso López Romo

# Participants

- APODACA ESPINOZA JUAN LUIS
- CORDOVA AVILA BRUNO
- CORONA AYALA MAXIMILIANO TADEO
- MORAN ROMERO JOSÉ FRANCISCO
- PATIÑO DORAME JOSE ALBERTO
- RIVERA VALENZUELA RAMON FRANCISCO
- RUIZ PLASCENCIA OSCAR EDUARDO



# How to

1. Clonar el repositorio y analizarlo.
2. Abrir el archivo `examenRecuperacion-appsWeb.mdj` y tomar el diagrama de clases como referencia.
3. Decide cual grupo de clases programarás.
4. Haz una rama nueva del repositorio, obviamente con el comando: `git checkout -b tu-nombre-y-apellido`. (Por ejemplo: `jose-patino` SIN ACENTOS, NI Ñ)
5. Para acreditar el examen tienes que programar clases que vengan en 3 colores diferentes, relacionadas entre si, del mismo paquete.
6. Utiliza la misma estructura de paquetes(carpetas) que ya estan en el repositorio cuando lo clones.
7. Cuando termines has un `push` al repositorio para entregar y que seas evaluado: `git push origin tu-nombre-y-apellido`
8. Listo.

# Evaluación:

- Solamente se recibe tu examen y se evaluarán las respuestas recibidas por `push`de `git`a este repositorio de `gitlab`

- Se te notificará al día siguiente tu estado de aprobación

- Si no acreditas, el extraordinario se aplicarà el jueves 27 de agosto. se te contactará en privado para la aplicación y los detalles.

- ¡Aplícate y que tengas el mejor resultado!

